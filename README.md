# CDISC-ODM transformation to transmart-batch format
This job transforms CDISC-ODM files into a folder for upload to tranSMART with transmart-batch. The ontology (tree structure) is given by the ODM hierarchy of forms, items, etc.; recurring items are consecutively numbered. 

## Setup

Import job in a current version of [Talend Open Studio for Data Integration](https://www.talend.com/products/data-integration-manuals-release-notes/) by right-clicking "Job Designs" in the Repository view and selecting "Import items". Select the directory "LOCAL_PROJECT" of your cloned (or downloaded) copy of this git repostiroy as your "root directory", activate "Overwrite existing items" and press "finish". There should be one new folder in your "Job Designs" tree: "ODMtoTmBatch".

### Before the first Start

Dedicate a directory on your local drive as the working directory for this job (example: "C:/Data/ODM2TranSMART/"). This directory will be the "basedir". Create two directories <ODM> and <computeValues>. Add your own CDISC-ODM file to <ODM>. Add "newValues.csv" to <computeValues>. And add the XSLT files from https://gitlab.gwdg.de/medinfpub/transmart/cdisc-odm-xslt-transformations to your basedir. Your local folder structure should now look like this:

```
<basedir>
│   IDRT.xslt
|   no_namespaces.xslt
|   no_namespaces_and_no_secuTrial_metadata.xslt
├── <computeValues>
    └── newValues.csv
├── <ODM>
    └── ODMxxx.xml
```

In the job "ODMtoTmBatch/ODM_Batch_MASTER" adjust the local path in the context variable "basedir" to your working directory. Always include a slash at the end.


### Initializing/Changing a specific Study

Before starting the job, your have to configure your Context for your specific CDISC-ODM file. In "ODM_Batch_MASTER" adjust the context (new contexts can be generated for each study):

| context | description | example |
| ------ | ------ | ------ |
| ageOID | OID from ODM file, where birthdate of patient is given | "FF.xxx" |
| genderOID | OID from ODM file with gender of patient | "FF.xxx" |
| birthdate_missingDay | if only the day is missing in birthdate, it is set to 15 | true/false |
| birthdate_missingMonthDay | if only year of birth is given, default 6/15 is set | true/false |
| ontRootLevel | top node in tranSMART ontology (tree) | "StudyXY" |
| ontAddLevels | additional nodes under root, e.g. StudyXY+secuTrial+2020; more levels than one are divided by "+" sign | "secuTrial+2020" |
| displayedRepetitions | how many repetitions are displayed on same level before using subfolders | 2 |


### Calculate New Values
An automatic calculation of the difference between two dates is included in the job. The new values are added to transmart-data.csv.
First, add newValues.xlsx to directory \<basedir>/\<computeValues>, then add your new values with their ODM OIDs to the file.

defaults: 
- unit: year ("y")
- format: "yyyy-MM-dd"


## Folder Structure
The following folder structure is created by the Job. Files in \<basedir>, \<computeValues> and \<ODM> have to be put there manually (s. Setup).

```
<basedir>
│   IDRT.xslt
|   no_namespaces.xslt
|   no_namespaces_and_no_secuTrial_metadata.xslt
├── <computeValues>
    └── newValues.csv
├── <ODM>
    └── ODMxxx.xml
├── <Output>
    |   codelist.csv
    |   data.csv
    |   demographics.csv
    |   no_namespaces.xml
    |   Only_PD.xml
    |   ont.csv
    |   repeatKeyCount.csv
    |   transmart_data.csv
    |   transmart_mapping.csv
    |   transmart_ont_tmp.csv
    |   transmart_tmp.csv
    ├── <_tmBatch_archive>
    |   └── ...
    └── <tmBatch>
        ├── <study1>
        ├── <study2>
        └── ...
└── <folderTmp>
```
